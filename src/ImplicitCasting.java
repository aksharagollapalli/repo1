public class ImplicitCasting {
    public static void main(String[] args) {

        double x = 1.1;
        int y = (int)x+2;
        System.out.println(y);
    }
}
