public class ForEach {
    public static void main(String[] args) {
        String[] fruits = {"apple", "mango", "orange"};

        for ( String fruit : fruits)
            System.out.println(fruit);

        for (int i=0; i<fruits.length; i++)
            System.out.println(fruits[i]);
    }

}
